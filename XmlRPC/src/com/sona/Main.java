package com.sona;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Exchanger;

import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcTransport;
import org.apache.xmlrpc.client.XmlRpcTransportFactory;


public class Main {

    public static void main(String[] args) throws MalformedURLException, XmlRpcException {

        //callRPC_addContact();
        call_optInEmail();
        callRPC_sendEmail();

    }

    private static void call_optInEmail() throws MalformedURLException {
//        privateKey: string (required)
//        Your Infusionsoft API key
//
//        email: string (required)
//        The email address to opt-in
//
//        optInReason: string (required)
//        Why/how this email was opted-in

        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        config.setServerURL(new URL(server_url));
        final XmlRpcClient client = new XmlRpcClient();
        final XmlRpcTransportFactory transportFactory = new XmlRpcTransportFactory()
        {
            public XmlRpcTransport getTransport()
            {
                return new MessageLoggingTransport(client);
            }
        };
        client.setTransportFactory(transportFactory);
        client.setConfig(config);

        /*************************************************
         *                                               *
         Opt in Email
         *                                               *
         *************************************************/
        List parameters = new ArrayList();
        parameters.add(key);
        parameters.add("ravi@sonagroup.net");
        parameters.add("You are a member of ideas centre group");

        try {
            boolean optinEmail = (boolean) client.execute("APIEmailService.optIn", parameters);
            System.out.println("Email optin for ravi@sonagroup.net " +optinEmail);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            System.out.println("client:"+client.toString());
        }


    }


    private static void callRPC_sendEmail() throws MalformedURLException, XmlRpcException {
        //Sets up the java client, including the api url
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        config.setServerURL(new URL(server_url));
        final XmlRpcClient client = new XmlRpcClient();
        final XmlRpcTransportFactory transportFactory = new XmlRpcTransportFactory()
        {
            public XmlRpcTransport getTransport()
            {
                return new MessageLoggingTransport(client);
            }
        };
        client.setTransportFactory(transportFactory);
        client.setConfig(config);

        /*************************************************
         *                                               *
         Send Email
         *                                               *
         *************************************************/
        List parameters = new ArrayList();
        parameters.add(key);

        Integer[] contactIds = new Integer[]{8163};
        parameters.add(contactIds);
        parameters.add("deepthi@sonagroup.net");
        parameters.add("ravi@sonagroup.net");
        parameters.add("a@gmail.com");
        parameters.add("a@gmail.com");
        parameters.add("text");
        parameters.add("hello");
        parameters.add("htmlBody");
        parameters.add("textBody");

        System.out.println("Parameters:"+parameters.toString());

        try {
            boolean emailSent = (boolean) client.execute("APIEmailService.sendEmail", parameters);
            System.out.println("Email send " +emailSent +"to:ravi@sonagroup.net");

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            System.out.println("client:"+client.toString());
        }



    }

    // The location of our server.https://api.infusionsoft.com/crm/xmlrpc/v1
    private final static String server_url = "https://api.infusionsoft.com/crm/xmlrpc/v1?access_token=53mvjt3chxkrnx67s924gmpx";
    static String key = "ge9d83drxxp94s46zjwzdgur";


    private static void callRPC_addContact() throws MalformedURLException, XmlRpcException {

        //Sets up the java client, including the api url
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        config.setServerURL(new URL(server_url));
        XmlRpcClient client = new XmlRpcClient();
        client.setConfig(config);


        /*************************************************
         *                                               *
         ADD CONTACT TO DATABASE
         *                                               *
         *************************************************/
        List parameters = new ArrayList();
        Map contactData = new HashMap();
        contactData.put("FirstName", "R");
        contactData.put("LastName", "K");
        contactData.put("Email", "ravi@sonagroup.net");

        parameters.add(key); //The secure key
        parameters.add("Contact"); //The table we will be adding to
        parameters.add(contactData); //The data to be added

        //Make the call
        int contactId = (int) client.execute("DataService.add", parameters);
        System.out.println("Successfully added contact:"+contactId);
    }
}
