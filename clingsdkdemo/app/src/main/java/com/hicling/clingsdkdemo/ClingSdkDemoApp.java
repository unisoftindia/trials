package com.hicling.clingsdkdemo;

import com.hicling.clingsdk.ClingSdk;

import android.app.Application;

public class ClingSdkDemoApp extends Application {
	@Override 
	public void onCreate() {
	    super.onCreate();
	    
	    // this is test appkey and appsecret
	    // user should request your own key on our web page ( http://developers.hicling.com )
		ClingSdk.init(this, "HC0f57aaa6f71e4770", "e61d41a06b03ed338b912c0545b0da4a");
	}
}

