package com.deepra.move;

import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.deepra.move.dummy.ConnectThread;

import java.util.UUID;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BTDeviceInfoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BTDeviceInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BTDeviceInfoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static MainActivity mActivity;
    private static BTDevice mBTDevice;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private TextView mDeviceName;
    private TextView mDeviceAddress;
    private TextView mDeviceSend;
    private TextView mDeviceReceived;
    private Button mButtonConnect;

    public BTDeviceInfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @param mActivity
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BTDeviceInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BTDeviceInfoFragment newInstance(MainActivity activity, BTDevice btDevice, String param1, String param2) {

        BTDeviceInfoFragment fragment = new BTDeviceInfoFragment();
        mActivity = activity;
        mBTDevice = btDevice;
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        fillUIMembers();
        copyToUI();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_btdevice_info, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
//        else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void fillUIMembers() {
        mDeviceName = (TextView) mActivity.findViewById(R.id.device_name_text);
        mDeviceAddress = (TextView) mActivity.findViewById(R.id.device_address_text);
        mDeviceSend = (TextView) mActivity.findViewById(R.id.device_send_hex);
        mDeviceReceived = (TextView) mActivity.findViewById(R.id.device_receive_hex);
        mButtonConnect = (Button) mActivity.findViewById(R.id.btdevice_bonnect);

        mButtonConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connectToDevice();
            }
        });
    }
    private void copyToUI() {
        mDeviceName.setText(mBTDevice.mName);
        mDeviceAddress.setText(mBTDevice.mDeviceAddress);
    }

    private void connectToDevice() {
        ParcelUuid[] SERIAL_UUID = mBTDevice.mBluetoothDevice.getUuids();
        UUID uuid = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");//SERIAL_UUID[0].getUuid();//UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");
        ConnectThread connectThread = new ConnectThread(mBTDevice.mBluetoothDevice, BluetoothAdapter.getDefaultAdapter(), uuid);
        connectThread.start();
    }
}
