package com.deepra.move;

import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;

/**
 * Created by ravi-dev on 22/09/2016.
 */
public class BTDevice {
    BluetoothDevice mBluetoothDevice;
    String mName;
    String mDeviceAddress;

    BTDevice(BluetoothDevice device) {
        mBluetoothDevice = device;
        mName = mBluetoothDevice.getName();
        mDeviceAddress = mBluetoothDevice.getAddress();
    }

    @Override
    public String toString() {
        String deviceInfo = "";
        if(mName != null) {
            deviceInfo = mName;
        }
        else {
            deviceInfo = "Unknown device";
        }
        deviceInfo += ":"+mDeviceAddress;

        String deviceBTMajorClass
                = getBTMajorDeviceClass(mBluetoothDevice
                .getBluetoothClass()
                .getMajorDeviceClass());
        deviceInfo += ":" + deviceBTMajorClass;
        return deviceInfo;
    }

    private String getBTMajorDeviceClass(int major) {
        switch (major) {
            case BluetoothClass.Device.Major.AUDIO_VIDEO:
                return "AUDIO_VIDEO";
            case BluetoothClass.Device.Major.COMPUTER:
                return "COMPUTER";
            case BluetoothClass.Device.Major.HEALTH:
                return "HEALTH";
            case BluetoothClass.Device.Major.IMAGING:
                return "IMAGING";
            case BluetoothClass.Device.Major.MISC:
                return "MISC";
            case BluetoothClass.Device.Major.NETWORKING:
                return "NETWORKING";
            case BluetoothClass.Device.Major.PERIPHERAL:
                return "PERIPHERAL";
            case BluetoothClass.Device.Major.PHONE:
                return "PHONE";
            case BluetoothClass.Device.Major.TOY:
                return "TOY";
            case BluetoothClass.Device.Major.UNCATEGORIZED:
                return "UNCATEGORIZED";
            case BluetoothClass.Device.Major.WEARABLE:
                return "AUDIO_VIDEO";
            default:
                return "unknown!";
        }
    }
}
