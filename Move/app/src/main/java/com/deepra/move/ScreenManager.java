package com.deepra.move;



import android.app.Fragment;
import android.app.FragmentTransaction;

import java.util.Hashtable;

/**
 * Created by ravi-dev on 16/09/2016.
 */
public class ScreenManager {
    public static final int SCREEN_DEVICES = 1001;
    public static final int SCREEN_DEVICE_INFO = 1002;

    Hashtable<Integer, Fragment> mScreens;
    MainActivity mActivity;

    public ScreenManager(MainActivity activity) {
        mScreens = new Hashtable<>();
        mActivity = activity;
    }

//    public void navigate(int screenDevices) {
//
//        switch (screenDevices) {
//            case SCREEN_DEVICES : navigateToDevicesScreen(); break;
//            case SCREEN_DEVICE_INFO: navigateToDeviceInfo(); break;
//        }
//    }

    public void navigateToDevicesScreen() {
        Fragment devicesScreen = (Fragment) mScreens.get(SCREEN_DEVICES);

        if(mScreens.get(SCREEN_DEVICES) == null) {
            devicesScreen = DevicesScreenFragment.newInstance(mActivity, "param1", "param2");
            mScreens.put(SCREEN_DEVICES, devicesScreen);
        }
        FragmentTransaction ft = mActivity.getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, devicesScreen);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void navigateToDeviceInfo(BTDevice btDevice) {
        Fragment deviceInfoScreen = (Fragment) mScreens.get(SCREEN_DEVICE_INFO);

        if(mScreens.get(SCREEN_DEVICE_INFO) == null) {
            deviceInfoScreen = BTDeviceInfoFragment.newInstance(mActivity, btDevice, "param1", "param2");
            mScreens.put(SCREEN_DEVICE_INFO, deviceInfoScreen);
        }
        FragmentTransaction ft = mActivity.getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, deviceInfoScreen);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
    }

}
