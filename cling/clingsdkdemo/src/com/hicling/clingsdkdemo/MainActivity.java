package com.hicling.clingsdkdemo;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.hicling.clingsdk.ClingSdk;
import com.hicling.clingsdk.OnClingSdkListener;

import com.hicling.clingsdk.model.DeviceConfiguration;
import com.hicling.clingsdk.model.DeviceNotification;
import com.hicling.clingsdk.model.MinuteData;
import com.hicling.clingsdk.model.UserProfile;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.hicling.clingsdk.bleservice.BluetoothDeviceInfo;
import com.hicling.clingsdk.devicemodel.PERIPHERAL_DEVICE_INFO_CONTEXT;
import com.hicling.clingsdk.devicemodel.PERIPHERAL_USER_REMINDER_CONTEXT;
import com.hicling.clingsdk.devicemodel.PERIPHERAL_WEATHER_DATA;
import com.hicling.clingsdk.devicemodel.PERIPHERAL_WEATHER_TYPE;

public class MainActivity extends Activity {
	private final static String TAG = MainActivity.class.getSimpleName();
	
	static boolean mbClingSdkReady = false;
	
	private static MainActivity instance;
	
	private static PERIPHERAL_DEVICE_INFO_CONTEXT mDeviceInfo = null;
	
	private static FirmwareUpgradeFragment mUpgradeFragment = null;
	private static double mdUpgradingProgress;
	
	private final static int msg_Upgrading_Progresss = 1000;
	private static Handler mMsgHandler = new Handler () {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);

//			Log.d(TAG, "got handler msg: " + msg.what);
			switch (msg.what) {
			case msg_Upgrading_Progresss:
				if ( mUpgradeFragment != null ) {
					mUpgradeFragment.showFirmwareUpgradingProgress(mdUpgradingProgress);
				}

				break;
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		ClingSdk.setListener ( mClingListener );
		ClingSdk.start(this);
		ClingSdk.enableDebugMode(true);
		ClingSdk.mbEnableDebugMode =  true;
	}

	@Override
	protected void onDestroy () {
		ClingSdk.stop(this);
		super.onDestroy();
	}
	
	@Override
	protected void onResume () {
		super.onResume();
		ClingSdk.onResume(this);
	}
	
	@Override
	protected void onPause () {
		ClingSdk.onPause(this);
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			final View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);

			final EditText editTextUsername = (EditText) rootView.findViewById ( R.id.signinup_username );
			final EditText editTextPassword1 = (EditText) rootView.findViewById ( R.id.signinup_password1 );
			final EditText editTextPassword2 = (EditText) rootView.findViewById ( R.id.signinup_password2 );

//			editTextUsername.setText("testsdk@hicling.com");
//			editTextPassword1.setText("qwer1234");
			editTextUsername.setText("1@1.111");
			editTextPassword1.setText("11111111");

			Button btnSignIn = (Button) rootView.findViewById ( R.id.signin_btn );
			btnSignIn.setOnClickListener( new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if ( mbClingSdkReady ) {
						if ( editTextUsername.getText().toString().length() > 0 && editTextPassword1.getText().toString().length() > 0) {
							Log.d(TAG, "sign in: " + editTextUsername.getText().toString() + ", " + editTextPassword1.getText().toString());
							ClingSdk.signIn(editTextUsername.getText().toString(), editTextPassword1.getText().toString() );
						}
					} else {
						Log.d(TAG, "Cling sdk not ready, please try again");
					}
				}
			});
			
			Button btnSignUp = (Button) rootView.findViewById ( R.id.signup_btn );
			btnSignUp.setOnClickListener( new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if ( mbClingSdkReady ) {
						String password1 = editTextPassword1.getText().toString();
						String password2 = editTextPassword2.getText().toString();
						if ( editTextUsername.getText().toString().length() > 0 && password1.length() > 0 && 0 == password1.compareTo(password2)) {
							ClingSdk.signUp(editTextUsername.getText().toString(), password1 );
						}
					} else {
						Log.d(TAG, "Cling sdk not ready, please try again");
					}
				}
			});

			return rootView;
		}
	}
	
	private static ListView mListViewScanResult;
	public static class ScanFragment extends Fragment {
		public ScanFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			final View rootView = inflater.inflate(R.layout.fragment_scan, container,
					false);
			
			Button btnDereg = (Button) rootView.findViewById(R.id.deregister_btn);
			btnDereg.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if ( mbClingSdkReady ) {
						ClingSdk.deregisterDevice();
					}
				}
			});

			mListViewScanResult = (ListView) rootView.findViewById(R.id.scan_result_list);
			mListViewScanResult.setOnItemClickListener(new OnItemClickListener () {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					ListAdapter adapter = mListViewScanResult.getAdapter();
					Object obj = adapter.getItem(position);
					if ( obj != null ) {
						@SuppressWarnings("unchecked")
						Map<String, Object> mapItem = (Map<String, Object>) obj;
						String devname = (String) mapItem.get("DEVNAME");
						if ( mArrayListScanResult != null ) {
							for ( BluetoothDeviceInfo bleinfo : mArrayListScanResult ) {
								if ( bleinfo.getmBleDevice().getName().equals(devname)) {
									ClingSdk.stopScan();
									ClingSdk.registerDevice(bleinfo.getmBleDevice());
									break;
								}
							}
						}
						
						showDownloadPage ();
					}
				}
			});

			Button btnStartScan = (Button) rootView.findViewById ( R.id.scan_start_btn );
			if ( ClingSdk.isAccountBondWithCling() ) {
				String clingId = ClingSdk.getBondClingDeviceName();
				int clingType = ClingSdk.getClingDeviceType();
				String strPrefix = "U";
				switch ( clingType ) {
				case ClingSdk.CLING_DEVICE_TYPE_WATCH_1:
					strPrefix = "W";
					break;
				case ClingSdk.CLING_DEVICE_TYPE_BAND_1:
					strPrefix = "B1";
					break;
				case ClingSdk.CLING_DEVICE_TYPE_BAND_2:
					strPrefix = "B2";
					break;
				}
				btnStartScan.setText(strPrefix + " " + clingId);
			}

			btnStartScan.setOnClickListener( new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if ( ! ClingSdk.isAccountBondWithCling() ) {
//						ClingSdk.setClingDeviceType ( ClingSdk.CLING_DEVICE_TYPE_WATCH_1 );
						ClingSdk.setClingDeviceType ( ClingSdk.CLING_DEVICE_TYPE_BAND_3 );
					}

					if ( mbClingSdkReady ) {
						ClingSdk.stopScan();
						if ( ! mbDeviceConnected ) {
							ClingSdk.startScan( 10*60*1000 ); // 10 minutes
						} else {
							showDownloadPage ();
						}
					} else {
						Log.d(TAG, "Cling sdk not ready, please try again");
					}
				}
			});
			
			Button btnStopScan = (Button) rootView.findViewById ( R.id.scan_stop_btn );
			btnStopScan.setOnClickListener( new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if ( mbClingSdkReady ) {
						ClingSdk.stopScan();
					} else {
						Log.d(TAG, "Cling sdk not ready, please try again");
					}
				}
			});			

			return rootView;
		}
		
	}
	
	private void showApiTestPage () {
		runOnUiThread ( new Runnable () {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				getFragmentManager().beginTransaction()
				.replace(R.id.container, new ScanFragment()).commit();
			}
		});
	}
	
	private static ArrayList<BluetoothDeviceInfo> mArrayListScanResult;
	private void updateScanResultView (final ArrayList<BluetoothDeviceInfo> arrlistDevices) {
		runOnUiThread ( new Runnable () {
			@Override
			public void run() {
				if ( mListViewScanResult!= null && arrlistDevices != null ) {
					mArrayListScanResult = arrlistDevices;
					ArrayList<Map<String, Object>> contents = new ArrayList<Map<String, Object>> ();
					for ( BluetoothDeviceInfo bleinfo : arrlistDevices ) {
						Log.d(TAG, String.format("device: %s, rssi:%d", bleinfo.getmBleDevice().getName(), bleinfo.getmRssi()));
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("DEVNAME", bleinfo.getmBleDevice().getName());
						map.put("RSSI", String.valueOf(bleinfo.getmRssi())+"db");
						contents.add(map);
					}
					SimpleAdapter adapter = new SimpleAdapter (MainActivity.this, contents,  R.layout.list_scan_item, new String []{"DEVNAME", "RSSI"}, new int []{R.id.listitem_device_name, R.id.listitem_rssi});
					mListViewScanResult.setAdapter(adapter);
				}
			}
		});
	}

	private static void showDownloadPage () {
		instance.getFragmentManager().beginTransaction()
		.replace(R.id.container, new MinuteDataFragment()).addToBackStack(null).commit();
	}

	private static ListView mListViewData;
	public static class MinuteDataFragment extends Fragment {
		public MinuteDataFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			final View rootView = inflater.inflate(R.layout.fragment_data, container,
					false);

			mListViewData = (ListView) rootView.findViewById(R.id.data_result_list);
			
			Button btnConfig = (Button) rootView.findViewById ( R.id.config_device_btn );
			btnConfig.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if ( mbClingSdkReady ) {
						showConfigPage ();
					}					
				}
			});
			
			Button btnDownload = (Button) rootView.findViewById ( R.id.request_data_btn );
			btnDownload.setOnClickListener( new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if ( mbClingSdkReady ) {
						long endTime = System.currentTimeMillis()/1000;
						long startTime = endTime - 10 * 3600;
						ClingSdk.requestMinuteData(startTime, endTime);
					}
				}
			});
			
			Button btnGetLocal = (Button) rootView.findViewById ( R.id.get_local_data_btn );
			btnGetLocal.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					long endTime = System.currentTimeMillis() / 1000;
					long startTime = endTime - 60 * 60;
					ArrayList<MinuteData> arrlist = ClingSdk.getMinuteData(startTime, endTime);
					
					Log.d(TAG, "get minute data size: " + arrlist.size());
				}
			});
			
			Button btnUpgrade = (Button) rootView.findViewById ( R.id.firmware_upgrade_btn );
			btnUpgrade.setOnClickListener( new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Log.d(TAG, "clicked upgrade button");
					showUpgradePage ();
				}
			});

			return rootView;
		}
	};
	
	private void updateMinuteDataView ( final ArrayList<MinuteData> arrlistData) {
		runOnUiThread ( new Runnable () {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if ( mListViewData != null && arrlistData != null ) {
					ArrayList<Map<String, Object>> contents = new ArrayList<Map<String, Object>> ();
					for ( MinuteData minData : arrlistData ) {
//						Log.d(TAG, String.format("device: %s, rssi:%d", bleinfo.getmBleDevice().getName(), bleinfo.getmRssi()));
						Map<String, Object> map = new HashMap<String, Object>();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
						String strDate = sdf.format ( new Date ( minData.minuteTimeStamp * 1000 ) );
						map.put("DATE", strDate);
						map.put("VALUE", minData.toString());
						contents.add(map);
					}
					SimpleAdapter adapter = new SimpleAdapter (MainActivity.this, contents,  R.layout.list_data_item, new String []{"DATE", "VALUE"}, new int []{R.id.listitem_timestamp, R.id.listitem_minute_data});
					mListViewData.setAdapter(adapter);
				}
			}
			
		});
	}
	
	private static void showConfigPage () {
		instance.getFragmentManager().beginTransaction()
		.replace(R.id.container, new ConfigurationFragment()).addToBackStack(null).commit();
	}

	public static class ConfigurationFragment extends Fragment {
		public ConfigurationFragment () {
			
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			final View rootView = inflater.inflate(R.layout.fragment_config, container,
					false);

			Button btnConfigDev = (Button) rootView.findViewById ( R.id.device_configuration_btn );
			btnConfigDev.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if ( mbClingSdkReady ) {
						DeviceConfiguration devCfg = new DeviceConfiguration ();
						devCfg.bActFlipWristEn = 1;
						devCfg.bActHoldEn = 1;
						devCfg.bNavShakeWrist = 1;
						ClingSdk.setPerpheralConfiguration(devCfg);
						showToast("config done, see result on Cling device");
					}
				}
			});
			
			Button btnSmartNoti = (Button) rootView.findViewById ( R.id.Smart_notification_btn );
			btnSmartNoti.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {					
					if ( mbClingSdkReady ) {
						DeviceNotification devNotification = new DeviceNotification ();
						devNotification.incomingcall = 1;
						devNotification.missedcall = 1;
						devNotification.social = 1;
						ClingSdk.setPeripheralNotification(devNotification);
						showToast("notification done");
					}
				}
			});

			Button btnReminder = (Button) rootView.findViewById ( R.id.Reminder_btn );
			btnReminder.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {					
					if ( mbClingSdkReady ) {
						PERIPHERAL_USER_REMINDER_CONTEXT reminder = new PERIPHERAL_USER_REMINDER_CONTEXT ();
						reminder.hour = 10;
						reminder.minute = 20;
						reminder.name = "meeting";
						ClingSdk.addPerpheralReminderInfo(reminder);
						showToast("reminder done, see result on Cling device");
					}
				}
			});

			Button btnWeather = (Button) rootView.findViewById ( R.id.Weather_btn );
			btnWeather.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {					
					if ( mbClingSdkReady ) {
						ArrayList<PERIPHERAL_WEATHER_DATA> arrlistWeather = new ArrayList<PERIPHERAL_WEATHER_DATA> ();
						Calendar calendar = Calendar.getInstance();
						int day = calendar.get(Calendar.DAY_OF_MONTH);
						int month = calendar.get(Calendar.MONTH);
						PERIPHERAL_WEATHER_DATA weather = new PERIPHERAL_WEATHER_DATA ();
						weather.day = day;
						weather.month = month + 1;
						weather.temperature_high = 89;
						weather.temperature_low = 37;
						weather.type = PERIPHERAL_WEATHER_TYPE.PERIPHERAL_WEATHER_SNOWY;
						arrlistWeather.add(weather);
						// more weather forecast...
						// weather = new PERIPHERAL_WEATHER_DATA ();...
						// ...
						// arrlistWeather.add(weather);
						ClingSdk.setPeripheralWeatherInfo(arrlistWeather);
						showToast("weather done, see result on Cling device");
					}
				}
			});
			
			Button btnLanguage = (Button) rootView.findViewById ( R.id.Language_btn);
			btnLanguage.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {					
					if ( mbClingSdkReady ) {
						// note this configuration does not work on Cling Watch devices. 
						// Now only Cling Band devices support language configuration.
						ClingSdk.setPeripheralLanguage(ClingSdk.CLING_DEVICE_LANGUAGE_TYPE_ZH_CN);
						showToast("Language changed, see result on Cling band device");
					}
				}
			});

			return rootView;
		}
	}

	private static void showUpgradePage () {
		Log.d(TAG, "show upgrade page");
		mUpgradeFragment = new FirmwareUpgradeFragment ();
		instance.getFragmentManager().beginTransaction()
		.replace(R.id.container, mUpgradeFragment).addToBackStack(null).commit();
	}
	
	public static class FirmwareUpgradeFragment extends Fragment {
		private TextView txtvTitle;
		private ProgressBar pbar;
		private TextView txtvProgress;
		
		public FirmwareUpgradeFragment () {
			
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			final View rootView = inflater.inflate(R.layout.fragment_upgrade, container,
					false);
			
			Button btnRequestFirmware = (Button) rootView.findViewById(R.id.request_firmware_btn);
			btnRequestFirmware.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if ( mbClingSdkReady ) {
						ClingSdk.requestFirmwareUpgradeInfo ();
					}
				}
			});

			Button btnUpgrade = (Button) rootView.findViewById ( R.id.firmware_upgrading_btn);
			btnUpgrade.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {					
					if ( mbClingSdkReady ) {
						// note this configuration does not work on Cling Watch devices. 
						// Now only Cling Band devices support language configuration.
						if ( mDeviceInfo != null ) {
							ClingSdk.upgradeFirmware(mDeviceInfo.softwareVersion);
						}
					}
				}
			});
			
			txtvProgress = (TextView) rootView.findViewById(R.id.firmware_progress_txt);
			txtvTitle = (TextView) rootView.findViewById (R.id.firmware_indicator_txt);
			pbar = (ProgressBar) rootView.findViewById(R.id.firmware_progress_bar);
			pbar.setMax(100);

			return rootView;
		}
		
		public void showDownloadProgress ( long progress ) {
			txtvTitle.setText("downloading");
			txtvProgress.setText("" + progress);
		}
		
		public void showFirmwareUpgradingProgress ( double progress ) {
			txtvTitle.setText("Upgrading");
			txtvProgress.setText(String.format(Locale.US, "%.1f%%", progress * 100 ));
			pbar.setProgress((int) (progress * 100) );
		}
	}
	
	private static void showToast (final String text) {
		instance.runOnUiThread ( new Runnable () {
			@Override
			public void run() {
				Toast.makeText(instance, text, Toast.LENGTH_SHORT).show();
			}
		});
	}

	private static boolean mbDeviceConnected = false;
	private OnClingSdkListener mClingListener = new OnClingSdkListener () {
		@Override
		public void onBleScanUpdated(ArrayList<BluetoothDeviceInfo> arg0) {
			// TODO Auto-generated method stub
			Log.d(TAG, "onBleScanUpdated()");
			updateScanResultView (arg0);
		}

		@Override
		public void onClingSdkReady() {
			// TODO Auto-generated method stub
			Log.d(TAG, "onClingSdkReady()");
			mbClingSdkReady = true;
		}

		@Override
		public void onDeregisterDeviceFailed(String arg0) {
			// TODO Auto-generated method stub
			Log.d(TAG, "onDeregisterDeviceFailed()");			
		}

		@Override
		public void onDeregisterDeviceFinished() {
			// TODO Auto-generated method stub
			Log.d(TAG, "onDeregisterDeviceFinished()");
			showToast ( "Deregister Device Finished" );
		}

		@Override
		public void onDeviceConnected() {
			// TODO Auto-generated method stub
			Log.d(TAG, "onDeviceConnected()");
			showToast ( "Device Connected" );
			mbDeviceConnected = true;
		}

		@Override
		public void onDeviceDisconnected() {
			// TODO Auto-generated method stub
			Log.d(TAG, "onDeviceDisconnected()");	
			mbDeviceConnected = false;
		}

		@Override
		public void onLoginFailed(String arg0) {
			// TODO Auto-generated method stub
			Log.d(TAG, "onLoginFailed(): " + arg0);
		}

		@Override
		public void onLoginSucceeded() {
			// TODO Auto-generated method stub
			Log.d(TAG, "onLoginSucceeded()");
			showApiTestPage ();
		}

		@Override
		public void onLogoutFailed(String arg0) {
			// TODO Auto-generated method stub
			Log.d(TAG, "onLogoutFailed()");			
		}

		@Override
		public void onLogoutSucceeded() {
			// TODO Auto-generated method stub
			Log.d(TAG, "onLogoutSucceeded()");			
		}

		@Override
		public void onRegisterDeviceFailed(String arg0) {
			// TODO Auto-generated method stub
			Log.d(TAG, "onRegisterDeviceFailed()");			
		}

		@Override
		public void onRequestMinuteDataFailed(String arg0) {
			// TODO Auto-generated method stub
			Log.d(TAG, "onRequestMinuteDataFailed()");
		}

		@Override
		public void onRequestMinuteDataReady(ArrayList<MinuteData> arg0) {
			// TODO Auto-generated method stub
			Log.d(TAG, "onRequestMinuteDataReady()");
			if ( arg0 != null ) {
				for ( MinuteData md : arg0 ) {
					Log.d(TAG, md.toString() );
					updateMinuteDataView (arg0);
				}
			}
		}

		@Override
		public void onGetUserProfileFailed(String arg0) {
			Log.d(TAG, "onGetUserProfileFailed " + arg0 );
		}

		@Override
		public void onGetUserProfileSucceeded(UserProfile profile) {
			Log.d(TAG, "onGetUserProfileSucceeded " + profile );
		}

		@Override
		public void onSetUserProfileFailed(String arg0) {
			Log.d(TAG, "onSetUserProfileFailed " + arg0 );
		}

		@Override
		public void onSetUserProfileSucceeded() {
			Log.d(TAG, "onSetUserProfileSucceeded" );
		}

		@Override
		public void onGotSosMessage() {
			Log.d(TAG, "received sos message");
			showToast ( "received sos message" );
		}

		@Override
		public void onDataSyncedFromDevice() {
			Log.d(TAG, "data synced");
			showToast ( "data synced" );
		}

		@Override
		public void onDataSyncingProgress(float arg0) {
			Log.d(TAG, "onDataSyncingProgress " + arg0 );
		}

		@Override
		public void onDeviceInfoReceived(PERIPHERAL_DEVICE_INFO_CONTEXT arg0) {
			mDeviceInfo = arg0;
			Log.d(TAG, "onDeviceInfoReceived: " + mDeviceInfo.softwareVersion );
		}

		@Override
		public void onFirmwareDownloadFailed(String arg0) {
			Log.d(TAG, "onFirmwareDownloadFailed " + arg0);
		}

		@Override
		public void onFirmwareDownloadProgress(long arg0) {
//			Log.d(TAG, "onFirmwareDownloadProgress " + arg0);
//			final long progress = arg0;
//			if ( mUpgradeFragment != null ) {
//				instance.runOnUiThread(new Runnable () {
//					@Override
//					public void run() {
//						mUpgradeFragment.showDownloadProgress(progress);
//					}
//				});
//			}
		}

		@Override
		public void onFirmwareDownloadSucceeded() {
			Log.d(TAG, "onFirmwareDownloadSucceeded ");
		}

		@Override
		public void onFirmwareInfoFailed(String arg0) {
			Log.d(TAG, "onFirmwareInfoFailed "+ arg0);
		}

		@Override
		public void onFirmwareInfoReceived(String arg0) {
			Log.d(TAG, "onFirmwareInfoReceived "+ arg0);
		}

		@Override
		public void onFirmwareSpaceNotEnough() {
			Log.d(TAG, "onFirmwareSpaceNotEnough" );
		}

		@Override
		public void onFirmwareUpgradeFailed(int arg0) {
			Log.d(TAG, "onFirmwareUpgradeFailed " + arg0 );
			showToast ( "upgrading failed " + arg0 );
		}

		@Override
		public void onFirmwareUpgradeProgress(double arg0) {
//			Log.d(TAG, "onFirmwareUpgradeProgress " + arg0 );
			mdUpgradingProgress = arg0;
			mMsgHandler.sendMessage(mMsgHandler.obtainMessage(msg_Upgrading_Progresss)); 
		}

		@Override
		public void onFirmwareUpgradeSucceeded() {
			Log.d(TAG, "onFirmwareUpgradeSucceeded" );
		}

		@Override
		public void onDataSyncingMinuteData(MinuteData arg0) {
			Log.d(TAG, arg0.toString());
		}
	};
}
